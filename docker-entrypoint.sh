#!/bin/bash

get_envs() {
	if [ -z "${DO_AUTH_TOKEN}" ]; then
		echo -n 'Digital Ocean auth token: '
		read DO_AUTH_TOKEN
		export DO_AUTH_TOKEN
	fi

	if [ -z "${DO_SSH_KEY_LABEL}" ]; then
		export DO_SSH_KEY_LABEL="kf2-server-$(dd if=/dev/urandom bs=1 count=4 2>/dev/null | hexdump -e '"%x"')"
	fi

	if [ -z "${DO_DROPLET_SIZE}" ]; then
		echo -n 'Digital Ocean droplet size: '
		read DO_DROPLET_SIZE
		export DO_DROPLET_SIZE
	fi

	if [ -z "${DO_DROPLET_REGION}" ]; then
		echo -n 'Digital Ocean droplet region: '
		read DO_DROPLET_REGION
		export DO_DROPLET_REGION
	fi

	if [ -z "${DO_DROPLET_NAME}" ]; then
		echo -n 'Digital Ocean droplet name: '
		read DO_DROPLET_NAME
		export DO_DROPLET_NAME
	fi

	# verify SSH keys are in place
	mkdir -p "${HOME}/.ssh"

	SSH_KEY_FILE=$(find "${HOME}/.ssh/" -name 'id_*' | grep -v '.pub$' | head -n 1)
	SSH_PUBLIC_KEY_FILE="${SSH_KEY_FILE}.pub"
	if [ -z "${SSH_KEY_FILE}" ] || ! [ -e "${SSH_PUBLIC_KEY_FILE}" ]; then
		rm -rf "${HOME}/.ssh/*"
		SSH_KEY_FILE="${HOME}/.ssh/id_ed25519"
		ssh-keygen -N '' -t ed25519 -f "${SSH_KEY_FILE}"
		SSH_PUBLIC_KEY_FILE="${SSH_KEY_FILE}.pub"
	fi

	SSH_KEY_FINGERPRINT=$(ssh-keygen -E md5 -l -f "${SSH_PUBLIC_KEY_FILE}" | awk '{ print $2 }' | sed 's/^MD5://')
}

check_do_auth() {
	doctl auth init --access-token "${DO_AUTH_TOKEN}"
	return $?
}

opt_help() {
	echo 'kf2-server'
	echo '=========='
	echo ' start  Creates a droplet with the given settings'
	echo ' kill   Forcibly kills & destroys a droplet with the given settings'
}

opt_start() {
	set -e

	get_envs

	check_do_auth

	if doctl compute droplet list --no-header --format Name | grep "${DO_DROPLET_NAME}"; then
		>&2 echo "Droplet with name '${DO_DROPLET_NAME}' already exists"
		return 1
	fi

	if ! doctl compute ssh-key list | grep -q "${SSH_KEY_FINGERPRINT}"; then
		echo "Adding SSH Key (${DO_SSH_KEY_LABEL}) ${SSH_KEY_FINGERPRINT}"
		doctl compute ssh-key create \
			"${DO_SSH_KEY_LABEL}" \
			--public-key \
			"$(cat "${SSH_PUBLIC_KEY_FILE}")"
	fi

	echo "Creating Droplet (and waiting for it to complete)"
	local IP_ADDRESS=$(doctl compute droplet create \
		"${DO_DROPLET_NAME}" \
		--image coreos-stable \
		--region "${DO_DROPLET_REGION}" \
		--size "${DO_DROPLET_SIZE}" \
		--ssh-keys "${SSH_KEY_FINGERPRINT}" \
		--no-header \
		--format PublicIPv4 \
		--wait)

	echo -n "waiting for ${IP_ADDRESS}:22 to be reachable"
	while ! nc -w 5 -z ${IP_ADDRESS} 22 &> /dev/null; do
		sleep 1
		echo -n "."
	done
	echo

	echo "Installing docker-compose"
	ssh -o 'StrictHostKeyChecking=no' core@${IP_ADDRESS} bash -c "true; curl -SsL \$(curl -SsL https://api.github.com/repos/docker/compose/releases/latest | jq -r '.assets[].browser_download_url' | grep 'Linux-x86_64$') > docker-compose && chmod +x docker-compose"

	echo "Copying docker-compose.yml over"
	ssh -o 'StrictHostKeyChecking=no' core@${IP_ADDRESS} bash -c "cat > docker-compose.yml" < /docker-compose.yml

	echo "Bringing up docker-compose"
	ssh -o 'SendEnv=*' -o 'StrictHostKeyChecking=no' core@${IP_ADDRESS} ./docker-compose up -d

	set +ex

	echo "following the logs for 2 minutes"
	timeout -t 120 ssh -o 'StrictHostKeyChecking=no' -t core@${IP_ADDRESS} ./docker-compose logs --follow

	echo
	echo "Droplet ${DO_DROPLET_NAME}"
	echo "========="
	echo "ssh core@${IP_ADDRESS}"
	cat "${SSH_KEY_FILE}"

	# remove ssh-key when done
	#trap "doctl compute ssh-key delete -f ${SSH_KEY_FINGERPRINT}" EXIT

	return 0
}

opt_kill() {
	set -e

	get_envs

	check_do_auth

	set +e

	# we don't care if this fails
	echo "Removing SSH key ${SSH_KEY_FINGERPRINT}"
	doctl compute ssh-key delete -f "${SSH_KEY_FINGERPRINT}"

	# we care if this fails
	echo "Removing droplet ${DO_DROPLET_NAME}"
	doctl compute droplet delete -f "${DO_DROPLET_NAME}"

	return $?
}

if ! [ -e /.dockerenv ]; then
	>&2 echo 'Must be running inside docker'
	exit 1
fi

case "${1}" in
	"start")
		opt_start
		;;

	"kill")
		opt_kill
		;;

	*)
		opt_help
		;;
esac

exit $?
