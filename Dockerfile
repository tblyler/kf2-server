FROM golang:1-alpine AS doctl

RUN apk add --no-cache git \
	&& go get -u github.com/digitalocean/doctl/cmd/doctl \
	&& mv ${GOPATH}/bin/doctl /bin/doctl

FROM alpine:3.7

# get the doctl build
COPY --from=doctl /bin/doctl /bin/doctl

RUN apk add --no-cache bash openssh-client ca-certificates

COPY ./docker-entrypoint.sh /bin/docker-entrypoint
COPY ./docker-compose.yml /docker-compose.yml

ENTRYPOINT ["docker-entrypoint"]
