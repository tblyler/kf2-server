package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"
	"unicode"

	"golang.org/x/net/html"
)

const (
	mapPrefix            = "KF-"
	mapNameHTMLTokenName = "div"
	mapNameHTMLAttrKey   = "class"
	mapNameHTMLAttrVal   = "workshopItemTitle"
)

type kf2Map struct {
	Name string
	ID   string
}

type kf2MapList struct {
	maps     []*kf2Map
	mapsLock sync.Mutex
}

func (k *kf2MapList) AddMap(kf2Map *kf2Map) {
	k.mapsLock.Lock()
	k.maps = append(k.maps, kf2Map)
	k.mapsLock.Unlock()
}

func (k *kf2MapList) GetMaps() []*kf2Map {
	k.mapsLock.Lock()
	defer k.mapsLock.Unlock()
	return k.maps[:]
}

func makeMapName(name string) string {
	words := strings.FieldsFunc(name, func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	})

	if words[0][:2] == "KF" {
		words[0] = words[0][2:]
	}

	output := mapPrefix
	for _, word := range words {
		output += strings.Title(word)
	}

	return output
}

func main() {
	urlListFile := ""
	kfEngineINI := ""
	kfGameINI := ""
	flag.StringVar(&urlListFile, "url-list", urlListFile, "The file containg a line-delimited list of steam community urls for maps, don't set this to use STDIN")
	flag.StringVar(&kfEngineINI, "kfengine-ini", kfEngineINI, "The file to append the KFEngine.ini settings to, don't set this to use STDOUT")
	flag.StringVar(&kfGameINI, "kfgame-ini", kfGameINI, "The file to append the KFGame.ini settings to, don't set this to use STDOUT")
	flag.Parse()

	var reader *bufio.Reader
	if urlListFile == "" {
		reader = bufio.NewReader(os.Stdin)
	} else {
		file, err := os.Open(urlListFile)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to open url list", urlListFile, err)
			return
		}

		defer file.Close()
		reader = bufio.NewReader(file)
	}

	nextSteamURL := func() string {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				fmt.Fprintln(os.Stderr, "Failed to read line from map list:", err)
			}

			return ""
		}

		return strings.TrimSpace(line)
	}

	maps := new(kf2MapList)
	wg := sync.WaitGroup{}

	for {
		steamURL := nextSteamURL()
		if steamURL == "" {
			break
		}

		wg.Add(1)
		go func() {
			defer wg.Done()

			kf2Map := new(kf2Map)

			url, err := url.Parse(steamURL)
			if err != nil {
				fmt.Fprintln(os.Stderr, "Failed to parse URL", steamURL, err)
				return
			}

			kf2Map.ID = url.Query().Get("id")
			if kf2Map.ID == "" {
				fmt.Fprintln(os.Stderr, "Failed to find id in URL", steamURL)
				return
			}

			resp, err := http.Get(steamURL)
			if err != nil {
				fmt.Fprintln(os.Stderr, "Failed to get map", steamURL, err)
				return
			}

			tokenizer := html.NewTokenizer(resp.Body)

			for kf2Map.Name == "" {
				tokenType := tokenizer.Next()
				if tokenType == html.ErrorToken {
					fmt.Fprintln(os.Stderr, "Failed to parse HTML for map", steamURL, tokenizer.Err())
					break
				}

				if tokenType == html.StartTagToken {
					tokenName, _ := tokenizer.TagName()
					if string(tokenName) == mapNameHTMLTokenName {
						for {
							attrKey, attrVal, moreAttr := tokenizer.TagAttr()
							if string(attrKey) == mapNameHTMLAttrKey && string(attrVal) == mapNameHTMLAttrVal {
								// we found our element!
								subTokenType := tokenizer.Next()
								if subTokenType != html.TextToken {
									// expected text token after workshop item title start token, got something bad
									// try to keep searching
									break
								}

								kf2Map.Name = string(tokenizer.Text())
								break
							}

							if !moreAttr {
								break
							}
						}
					}
				}
			}

			kf2Map.Name = makeMapName(kf2Map.Name)

			maps.AddMap(kf2Map)
		}()
	}

	wg.Wait()

	// kfEngine output
	var kfEngineWriter io.WriteCloser
	if kfEngineINI == "" {
		kfEngineWriter = os.Stdout
	} else {
		var err error
		kfEngineWriter, err = os.OpenFile(kfEngineINI, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to open kf engine ini file", kfEngineINI, err)
			return
		}
	}

	defer kfEngineWriter.Close()

	_, err := fmt.Fprintln(kfEngineWriter, "[OnlineSubsystemSteamworks.KFWorkshopSteamworks]")
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to write to kfengine.ini", err)
		return
	}

	for _, kf2Map := range maps.GetMaps() {
		_, err = fmt.Fprintln(kfEngineWriter, "ServerSubscribedWorkshopItems="+kf2Map.ID)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to write to kfengine.ini", err)
			return
		}
	}

	// kfGame output
	var kfGameWriter io.WriteCloser
	if kfGameINI == "" {
		kfGameWriter = os.Stdout
	} else {
		kfGameWriter, err = os.OpenFile(kfGameINI, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to open kf game ini file", kfGameINI, err)
			return
		}
	}

	defer kfGameWriter.Close()

	for _, kf2Map := range maps.GetMaps() {
		_, err = fmt.Fprintln(kfGameWriter, "["+kf2Map.Name+" KFMapSummary]")
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to write to kfgame.ini", err)
			return
		}

		_, err = fmt.Fprintln(kfGameWriter, "MapName="+kf2Map.Name)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to write to kfgame.ini", err)
			return
		}

		_, err = fmt.Fprintln(kfGameWriter, "ScreenshotPathName=UI_MapPreview_TEX.UI_MapPreview_Placeholder")
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to write to kfgame.ini", err)
			return
		}
	}

	if kfGameINI == "" {
		fmt.Fprintln(kfGameWriter, "[KFGame.KFGameInfo]")
		mapsString := ""
		for _, kf2Map := range maps.GetMaps() {
			mapsString += "\"" + kf2Map.Name + "\","
		}

		fmt.Fprintln(kfGameWriter, "GameMapCycles=\\(Maps=\\("+mapsString[:len(mapsString)-1]+"\\)\\)")
	} else {
		kfGameWriter.Close()

		kfGame, err := ioutil.ReadFile(kfGameINI)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to read kfgame.ini", kfGameINI, err)
			return
		}

		regex := regexp.MustCompile("GameMapCycles=\\(Maps=\\(.*")

		found := regex.Find(kfGame)
		if found == nil {
			// HACK just ignore me pretty much
			kfGameWriter, err = os.OpenFile(kfGameINI, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				fmt.Fprintln(os.Stderr, "Failed to open kf game ini file", kfGameINI, err)
				return
			}

			defer kfGameWriter.Close()

			mapsString := ""
			for _, kf2Map := range maps.GetMaps() {
				mapsString += "\"" + kf2Map.Name + "\","
			}
			fmt.Fprintln(kfGameWriter, "GameMapCycles=(Maps=("+mapsString[:len(mapsString)-1]+"))")
			return
		}

		foundMaps := string(found)
		currentMaps := strings.Split(foundMaps[strings.LastIndex(foundMaps, "(")+1:strings.Index(foundMaps, ")")], ",")

		for _, kf2Map := range maps.GetMaps() {
			currentMaps = append(currentMaps, "\""+kf2Map.Name+"\"")
		}

		kfGame = regex.ReplaceAll(kfGame, []byte("GameMapCycles=(Maps=("+strings.Join(currentMaps, ",")+"))"))

		ioutil.WriteFile(kfGameINI, kfGame, 0644)
	}
}
